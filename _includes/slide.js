function slide() {
    for (var t = document.querySelectorAll("img.slide-select"), e = 0; e < t.length; e++) t[e].addEventListener("click", slideThumbClick)
}

function slideThumbClick(t) {
    if (dest = document.getElementById(t.target.getAttribute("data-slide-dest")), dest) {
        dest.src = t.target.getAttribute("data-slide-img");
        var e = t.target.getAttribute("data-slide-zoom-img");
        if (e) {
            dest.removeAttribute("data-zoom-loaded"), dest.setAttribute("data-zoom-img", e), actives = t.target.parentElement.getElementsByClassName("active");
            for (var a = 0; a < actives.length; a++) removeClass(actives[a], "active");
            t.target.className += " active"
        }

        var caption = t.target.getAttribute("title");
        if (caption) {
            dest.parentElement.getElementsByTagName("figcaption")[0].innerHTML = caption;
        }
    }
}