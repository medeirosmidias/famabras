function toggleCollapse(obj) {
	var menu = document.getElementById(obj.getAttribute('data-toggle-dest'));
	if(menu) {
		if (hasClass(menu, 'collapsed')) {
			expandSection(menu, obj);
		} else {
			collapseSection(menu, obj);
		}
	}
}

function collapseSection(element, p) {
	var sectionHeight = element.scrollHeight;
	var elementTransition = element.style.transition;
	element.style.transition = '';
	requestAnimationFrame(function() {
		element.style.height = sectionHeight + 'px';
		element.style.transition = elementTransition;
		requestAnimationFrame(function() {
			element.style.height = 0 + 'px';
			requestAnimationFrame(function() {
				element.style.height = null;
				element.className += ' collapsed';
			});
		});
	});
	removeClass(p, 'toggle-expanded');
	p.className += ' toggle-collapsed';
}

function expandSection(element, p) {
	element.style.height = element.scrollHeight + 'px';
	element.addEventListener('transitionend', function(e) {
		element.removeEventListener('transitionend', arguments.callee);
		element.style.height = null;
	});
	removeClass(element, 'collapsed');
	removeClass(p, 'toggle-collapsed');
	p.className += ' toggle-expanded';
}