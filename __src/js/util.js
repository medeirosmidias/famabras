function addEvent(element, event_name, func) {
    if (element.addEventListener) {
        element.addEventListener(event_name, func, false); 
    } else if (element.attachEvent)  {
        element.attachEvent("on"+event_name, func);
    }
}

function removeClass(obj, sClassToRemove) {
    var aNewClasses = [],
      aOldClasses = obj.className.split(' ');
    for (var i=0, imax=aOldClasses.length; i<imax; ++i) {
      if (!aOldClasses[i]) continue;  // Account for class="a  b"
      if (aOldClasses[i]!==sClassToRemove) aNewClasses.push(aOldClasses[i]);
    }
    obj.className = aNewClasses.join(' ');
}
 
function hasClass(el, selector) {
   var className = " " + selector + " ";
   if ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1) {
    return true;
   }
   return false;
}