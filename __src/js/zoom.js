function zoom() {
    if (!1 === /Android|BlackBerry|iPhone|iPad|iPod|webOS/i.test(navigator.userAgent))
        for (var t = document.querySelectorAll("img.image-zoom"), e = 0; e < t.length; e++) t[e].addEventListener("mouseout", zoomOut), t[e].addEventListener("mousemove", zoomMove)
}

function zoomOut(t) {
    var e = document.getElementById("image-zoom-box-square");
    e && (e.style.display = "none");
    var o = document.getElementById(t.target.getAttribute("data-zoom-dest"));
    o && removeClass(o, "image-zoom-box-in")
}

function zoomMove(t) {
    var e = document.getElementById(t.target.getAttribute("data-zoom-dest"));
    if (e) {
        "" !== t.target.title && (t.target.title = ""), hasClass(e, "image-zoom-box-in") || (e.className += " image-zoom-box-in");
        var o = e.getElementsByTagName("img");
        0 === o.length ? ((o = new Image).src = t.target.getAttribute("data-zoom-img"), o.setAttribute("data-zoom-loaded", t.target.getAttribute("data-zoom-img")), e.appendChild(o)) : (o = o[0]).getAttribute("data-zoom-loaded") != t.target.getAttribute("data-zoom-img") && (o.setAttribute("data-zoom-loaded", t.target.getAttribute("data-zoom-img")), o.src = t.target.getAttribute("data-zoom-img"));
        var i = t.target.getBoundingClientRect().width,
            a = t.target.getBoundingClientRect().height,
            g = o.getBoundingClientRect().width,
            n = o.getBoundingClientRect().height,
            m = e.getBoundingClientRect().width,
            d = e.getBoundingClientRect().height,
            l = i / g * m,
            r = a / n * d;
        o.style.left = -((t.clientX - t.target.getBoundingClientRect().left) * g / i - m / 2) + "px", o.style.top = -((t.clientY - t.target.getBoundingClientRect().top) * n / a - d / 2) + "px";
        var u = document.getElementById("image-zoom-box-square");
        u || ((u = document.createElement("div")).id = "image-zoom-box-square", u.className += "image-zoom-box-square"), t.target.parentElement.appendChild(u), u.style.left = t.clientX - t.target.getBoundingClientRect().left - l / 2 + "px", u.style.top = t.clientY - t.target.getBoundingClientRect().top - r / 2 + "px", u.style.width = l + "px", u.style.height = r + "px", u.style.display = "block"
    }
}