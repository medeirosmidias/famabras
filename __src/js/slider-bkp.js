﻿/*
.slider {
	position: relative;
	overflow: hidden;
	.slide {
		position: absolute;
	}
	.slider-dots {
		position: absolute;
		z-index: 101;
		transform: translate(-50%, -50%);
		left: 50%;
		bottom: 0;
		span {
			cursor: pointer;
			color: white;
			opacity: .55;
			&.active {
				color: #007f00;
			}
			&:hover {
				color: #007f00;
				opacity: 1;
			}
		}
	}
}

.slider {
	cursor: default !important;
	height: 26vw;
	min-height: 260px;
	.slide {
		width: 100%;
		height: 26vw;
		min-height: 260px;
		line-height: 2em;
		color: white;
		text-shadow: #000 1px 1px;
		transition: left .7s ease-out;
		h1 {
			color: white;
			margin-top: 0;
			padding-top: 1.5em;
		}
		p {
			color: white;
		}
		a.btn {
			margin-top: 50px;
		}
	}
	.slide1 {
		background: gray url("{{ site.baseurl }}{% link /imagens/banner1.jpg %}") no-repeat bottom right;
		background-size: cover;
	}
	.slide2 {
		background: gray url("{{ site.baseurl }}{% link /imagens/banner2.jpg %}") no-repeat bottom right;
		background-size: cover;
		display: none;
	}
	.slide3 {
		background: gray url("{{ site.baseurl }}{% link /imagens/banner3.jpg %}") no-repeat bottom right;
		background-size: cover;
		display: none;
	}
}
*/
var sliders = document.querySelectorAll('.slider');
if (sliders) {
	for (var i = 0; i < sliders.length; i++) {
		slider(sliders[i]);
	}
}

function slider(s) {
	this.slider = s;
	this.dots = s.querySelector('.slider-dots');
	this.slides = s.getElementsByClassName('slide');
	this.curSlide = 0;
	
	this.adjustzindex = function() {
		for(var i = 0; i < this.curSlide; i++) {
			this.slides[i].style.zIndex = 50 + i;
		}
		for(var i = this.curSlide; i < this.slides.length; i++) {
			this.slides[i].style.zIndex = 100 - i;
		}
	};

	this.dotClick = function(e) {
		if (!e) {
			e = window.event;
		}
		var sender = e.srcElement || e.target;
		goTo(parseInt(e.target.getAttribute('slide')));
	};
	
	this.next = function() {
		this.goTo(this.curSlide + 1 < this.slides.length ? this.curSlide + 1 : 0);
	};
	
	this.goTo = function(toSlide) {
		/* Ajusta o dot ativo */
		dots.children[this.curSlide].classList.remove('active');
		dots.children[toSlide].className += ' active';

		if (toSlide > this.curSlide) {
			var width = this.slides[this.curSlide].getBoundingClientRect().width;
			var diff = toSlide - this.curSlide;
			
			/* Reposiciona os elementos envolvidos na transição */
			for (var i = 0; i <= diff; i++) {
				this.slides[this.curSlide + i].style.transition = 'none';
				this.slides[this.curSlide + i].style.left = (i * width) + 'px';
			}
			
			/* Executa a transição */
			var cSlide = this.curSlide; // Cópia
			setTimeout(function() {
				for (var i = 0; i <= diff; i++) {
					this.slides[cSlide + i].style.transition = '';
					this.slides[cSlide + i].style.left = -((diff - i) * width) + 'px';
				}
			}, 10);
		} else if (toSlide < this.curSlide) {
			var width = this.slides[this.curSlide].getBoundingClientRect().width;
			var diff = this.curSlide - toSlide;
			
			/* Reposiciona os elementos envolvidos na transição */
			for (var i = 0; i <= diff; i++) {
				this.slides[this.curSlide - i].style.transition = 'none';
				this.slides[this.curSlide - i].style.left = (-i * width) + 'px';
			}
			
			/* Executa a transição */
			var cSlide = this.curSlide; // Cópia
			setTimeout(function() {
				for (var i = 0; i <= diff; i++) {
					this.slides[cSlide - i].style.transition = '';
					this.slides[cSlide - i].style.left = ((diff - i) * width) + 'px';
				}
			}, 10);
		}
		this.curSlide = toSlide;
		this.adjustzindex();
	};

	this.pos1 = 0;
	this.pos3 = 0;
	this.elmnt = null;
	
	this.closeDragElement = function(e) {
		document.onmouseup = null;
		document.onmousemove = null;			
	}

	this.dragMouseMove = function(e) {
		e = e || window.event;
		pos1 = pos3 - e.clientX;
		pos3 = e.clientX;
		elmnt.style.transition = 'none';
		elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
	}

	this.dragMouseDown = function(e) {
		e = e || window.event;
		elmnt = e.currentTarget;
		pos3 = e.clientX;
		document.onmouseup = closeDragElement;
		document.onmousemove = dragMouseMove;
	}
	
	var width = this.slides[0].getBoundingClientRect().width;
	dots.children[0].className += ' active';
	for (var i = 0; i < this.slides.length; i++) {
		var slide = this.slides[i];
		slide.style.transition = 'none';
		slide.style.left = (width * i) + 'px';
		slide.style.display = 'block';
		//slide.onmousedown = this.dragMouseDown;
	}

	this.adjustzindex();
	
	/* Configura os "pontos" para responder ao clique e trocar o slide */
	var x = dots.getElementsByTagName('span');
	for (var i = 0; i < x.length; i++) {
		x[i].setAttribute('slide', i);
		addEvent(x[i], 'click', this.dotClick);
	};
	
	this.autochange = function() {
		this.next();
	}
	
	this.interval = setInterval(this.autochange, 4000);
	
	addEvent(s, 'mouseenter', function(e) {
		clearInterval(interval);
	});

	addEvent(s, 'mouseleave', function(e) {
		interval = setInterval(autochange, 4000);
	});
};
