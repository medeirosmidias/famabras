@ECHO OFF

echo %1
set m=ImageMagick-7.0.7-4-portable-Q16-x64\magick.exe 

	%m% %1 -set filename:fname "%%t" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ^
		^( +clone -resize 600x -write ..\..\imagens\%%[filename:fname]-3.jpg ^) ^
		^( +clone -resize 300x -write ..\..\imagens\%%[filename:fname]-2.jpg ^) ^
		^( +clone -resize 160x -write ..\..\imagens\%%[filename:fname]-1.jpg ^) ^
		^( +clone -resize x120 -write ..\..\imagens\%%[filename:fname]-t2.jpg ^) ^
		^( +clone -resize 60x -write ..\..\imagens\%%[filename:fname]-t.jpg ^) ^
null:

	%m% %1 -set filename:fname "%%t" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB ^
		^( +clone -resize 1200x628 -background white -gravity center -extent 1200x628 -write ..\..\imagens\%%[filename:fname]-1200x628.jpg ^) ^
null:


pause