$text_ficha = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'ficha: '} | % { $_.Substring(7) }
$text_cat_p = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'cat-p: '} | % { $_.Substring(7) }
$text_cat_s = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'cat-s: '} | % { $_.Substring(7) }
$text_cat_t = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'cat-t: '} | % { $_.Substring(7) }
$text_fol_c = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'fol-c: '} | % { $_.Substring(7) }
$text_ver_w = Get-Content 'textos-utf8.txt' | Where-Object {$_ -match 'ver-w: '} | % { $_.Substring(7) }

'Catálogos...'

#Invoke-Expression ('.\exiftool.exe -overwrite_original -author="Famabras" -title="' + $text_cat_p + ' - Famabras" -subject="' + $text_cat_p + ' ' + $text_ver_w + ' - Famabras" -xmptoolkit= ..\..\pdf\catalogo-pressao.pdf')
Invoke-Expression ('.\exiftool.exe -overwrite_original -author="Famabras" -title="' + $text_cat_s + ' - Famabras" -subject="' + $text_cat_s + ' ' + $text_ver_w + ' - Famabras" -xmptoolkit= ..\..\pdf\catalogo-soldagem.pdf')
#Invoke-Expression ('.\exiftool.exe -overwrite_original -author="Famabras" -title="' + $text_cat_t + ' - Famabras" -subject="' + $text_cat_t + ' ' + $text_ver_w + ' - Famabras" -xmptoolkit= ..\..\pdf\catalogo-temperatura.pdf')
#Invoke-Expression ('.\exiftool.exe -overwrite_original -author="Famabras" -title="' + $text_fol_c + ' - Famabras" -subject="' + $text_fol_c + ' ' + $text_ver_w + ' - Famabras" -xmptoolkit= ..\..\pdf\folder-chopp-e-post-mix.pdf')

Get-ChildItem "..\..\_produtos\" -Filter *.html -Recurse | ? {$_.LastWriteTime -gt (Get-Date).AddDays(-1) } |
Foreach-Object {
    $content = Get-Content $_.FullName

    $description = $content | Where-Object {$_ -match 'description: '} | % { $_.Substring(13) }
    $pdf = $content | Where-Object {$_ -match '^pdf: '} | % { $_.Substring(5) }
    $title = $content | Where-Object {$_ -match '^title: '} | % { $_.Substring(7) }
    $subtitle = $content | Where-Object {$_ -match '^subtitle: '} | % { $_.Substring(10) }
    if($subtitle) {
        $title = $title + ' - ' + $subtitle
    }
    if($description -and $pdf) {
        $cmd = '.\exiftool.exe -overwrite_original -author="Famabras" -title="' + $fichatext + ' ' + $title + ' - Famabras" -subject="' + $description + '" -xmptoolkit= ..\..\pdf\' + $pdf 
        $title
        Invoke-Expression $cmd
    }
    
}
